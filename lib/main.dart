import 'package:cubit_practice/cubit_a.dart';
import 'package:cubit_practice/drawer.dart';
import 'package:cubit_practice/my_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cubit/flutter_cubit.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home:  BlocProvider(create:(_) => CubitA(/*Colors.purple*/), child: MyHomePage(title: 'Flutter Demo Home Page'),),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  /*@override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }*/

  Color backColor = Colors.grey;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CubitA, MyState>(builder: (context, state) {
      return Scaffold(
        appBar: AppBar(
          title: Text('widget.title'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'cubit',
              ),
            ],
          ),
        ),
        drawer: MainDrawer(),
        backgroundColor: /*backColor*/state.colorState,
      );
    });
  }
}
