import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyChangeNotifier extends ChangeNotifier{
  Color someColor;
  String field2;

  void changeState(){
    field2 = 'New value';
    notifyListeners();
  }
}