import 'package:bloc/bloc.dart';
import 'package:cubit_practice/my_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';



class CubitA extends Cubit<MyState> {

  /*final Color sColor;*/

  CubitA(/*this.sColor*/) : super(MyState(colorState: Colors.grey));

  void  setRed() => emit(MyState(colorState: Colors.red));
  void setGreen() => emit(MyState(colorState: Colors.green));
  void setBlue() => emit(MyState(colorState: Colors.blue));

}