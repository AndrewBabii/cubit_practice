import 'package:cubit_practice/cubit_a.dart';
import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            height: 100,
          ),
          FlatButton(
            onPressed: (){CubitA().setGreen();},
            child: Text('Green'),
            color: Colors.green,
          ),
          FlatButton(
            onPressed: (){CubitA().setRed();},
            child: Text('Red'),
            color: Colors.red,
          ),
          FlatButton(
            onPressed: (){CubitA().setBlue();},
            child: Text('Blue'),
            color: Colors.blue,
          ),

        ],
      ),
    );
  }
}
