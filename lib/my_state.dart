import 'package:bloc/bloc.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';





abstract class SomeState {
  const SomeState();
}

class SomeInitial extends SomeState {
  const SomeInitial();
}


class MyState{
  final Color colorState;
  final String f;

  MyState({this.colorState, this.f });
}


class MyCubit extends Cubit<MyState> {
  MyCubit() : super(MyState(colorState: Colors.red, f:'Initial value'));

  void changeState() {
    emit(MyState(colorState: Colors.red, f:'Initial value'));
  }
}